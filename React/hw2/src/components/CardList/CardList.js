import React, { PureComponent } from 'react'
import './CardList.scss'
import Card from "../Card/Card"
import PropTypes from "prop-types";
import axios from "axios";
import Loader from "../Loader/Loader";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";

class CardList extends PureComponent {

    state = {
        active: false,
        prods:[],
        isLoading: true,
        name: '',
        price: '',
        id:'',
    }

    componentDidMount() {
        setTimeout(() => {
            axios('/prods.json')
                .then(res => {
                    this.setState({ prods: res.data, isLoading: false })
                })
        }, 1000)
    }

    render() {
        const { active, prods, isLoading, name, price, id } = this.state;

        const prodCard = prods.map(e => <Card key={e.id} id={e.id} name={e.name} url={e.url} price={e.price} vendorCode={e.vendorCode} color={e.color} handAdd={this.modalActive} />)

        if (isLoading) {
            return <Loader />
        }

        return (
            <div className="body">
                <div className="body__container">
                    {prodCard}
                    {console.log("point 1")}
                    {active && <Modal textHeader={"Добавить в корзину?"}
                                      text={`Вы хотите купить бас-гитару ${name} всего лишь за ${price}?`}
                                      active={active}
                                      setActive={this.modalActive}
                                      closeButton={true}
                                      actions={[<Button key="1" classStyle="modal__btn" text="Ok"
                                                        handClick={() => {this.addOrder(id)}}/>,
                                          <Button key="2" classStyle="modal__btn" text="Cancel"
                                                  handClick={() => {this.removeOrder(id)}}/>]}
                    />}
                </div>
            </div>
        )
    }

    modalActive = (ProdID, ProdName, ProdPrice) => {

        this.setState(state => ({
            active: !this.state.active,
            name:ProdName,
            price:ProdPrice,
            id:ProdID
        }))
    }
    addOrder = (ProdID) => {

        let arr = [];
        if (localStorage.getItem('id')){
            arr = JSON.parse(localStorage.getItem('id'));
        }

        arr.push(ProdID);
        localStorage.setItem(
            "id",JSON.stringify(arr));

        this.setState(state => ({
            active: !this.state.active
        }))
    }
    removeOrder = (ProdID) =>{

        let arr = [];
        if (localStorage.getItem('id')){
            arr = JSON.parse(localStorage.getItem('id'));
            console.log(arr.indexOf(ProdID, 0));
            /*-Проверяем если нажали Cancel и такого элемента нет-*/
            if (arr.includes(ProdID, 0)){
            arr.splice(arr.indexOf(ProdID, 0), 1);
            localStorage.setItem(
                "id",JSON.stringify(arr));}
        }

        this.setState(state => ({
            active: !this.state.active
        }))

    }
}

CardList.propTypes = {
    prods: PropTypes.object,
    prodCard: PropTypes.node,
}

CardList.defaultProps = {
    prods: {
        id: "0",
        name: "товар не найден",
        url: "/",
        price: "00.00",
        vendorCode: "0000",
        color: "black"
    }
};

export default CardList