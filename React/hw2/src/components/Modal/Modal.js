import React from 'react';
import './Modal.scss'
import PropTypes from "prop-types";

class Modal extends React.Component {
    render() {
        const {text, textHeader, setActive, closeButton, actions} = this.props;
        return (
            <div className={"modal"} onClick={setActive}>
                <div className="modal__content" onClick={e=>e.stopPropagation()}>
                    <div className="modal__header">{textHeader}</div>
                    <span className="modal__close" onClick={setActive} style={closeButton ? {} : {display: "none"}}></span>
                    <p className="modal__text">{text}</p>
                    <div className="modal__section-btn">
                        {actions}
                    </div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    text: PropTypes.string,
    textHeader: PropTypes.string,
    setActive: PropTypes.func,
    closeButton: PropTypes.bool,
    actions: PropTypes.node,
}
export default Modal