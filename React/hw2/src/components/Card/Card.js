import React, {PureComponent} from 'react'
import './Card.scss'
import Button from "../Button/Button";
import PropTypes from "prop-types";

class Card extends PureComponent {
    state = {
        favorites: false,
    }

    componentDidMount() {
        let arr = [];
        arr = JSON.parse(localStorage.getItem('vendorCode'));
        if(!!arr && arr.includes(this.props.vendorCode, 0)){
            this.setState(state => ({
                favorites: true
            }))
        }
    }

    render() {
        const {id, name, url, price, vendorCode, color, handAdd} = this.props;
        const { favorites} = this.state

        let classStar = "card__star";
        if (favorites) {
            classStar += ' card__star_back'
        }
        return (

            <div className="card">
                <h2 className="card__title">{name}</h2>
                <div className={classStar} onClick={this.addFavorites}></div>
                <img className="card__img" src={url} alt={name}/>
                <span className="card__price">{price}</span>
                <span className="card__vendorCode">{vendorCode}</span>
                <span className="card__color">{color}</span>
                <Button classStyle="card__btn" text="Add to cart" handClick={()=>handAdd(id, name, price)}/>
            </div>
        )
    }

/*- arr.splice(randomNumber, 1); -*/
    addFavorites =() =>{
        let arr = [];
        if (localStorage.getItem('vendorCode')){
            arr = JSON.parse(localStorage.getItem('vendorCode'));
        }

        arr.push(this.props.vendorCode);

        if (this.state.favorites){
            arr = JSON.parse(localStorage.getItem('vendorCode'));
            arr.splice(arr.indexOf(this.props.vendorCode, 0), 1);
            localStorage.setItem(
                "vendorCode",JSON.stringify(arr));
        }
        else {
            localStorage.setItem(
                "vendorCode",JSON.stringify(arr));
        }

        this.setState(state => ({
            favorites: !this.state.favorites
        }))
    }

}

Card.propTypes = {
    name: PropTypes.string,
    url: PropTypes.string,
    price: PropTypes.string,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    active: PropTypes.bool,
    favorites: PropTypes.bool,
    classStar: PropTypes.string,
}

export default Card
