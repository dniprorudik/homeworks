import React, {PureComponent} from 'react'
import './Header.scss'


class Header extends PureComponent {

    render() {
        return (
            <div className="header">
                <div className="header__container">
                    <img className="header__img" src="img/spanch_bass.jpg" alt="logo"/>
                    <h1>Интернет магазин Бас гитар</h1>
                </div>
            </div>
        )
    }
}

export default Header