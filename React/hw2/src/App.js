import React from 'react';
import './App.scss';
import CardList from './components/CardList/CardList';
import Header from "./components/Header/Header";

class App extends React.Component {

       render() {
        return (
            <div>
                <Header/>
                <CardList />
            </div>
        );
    }

}

export default App;
