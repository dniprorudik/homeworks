import React, { PureComponent } from 'react'

function Loader () {
    return (
        <h2>Loading...</h2>
    )
}

export default Loader