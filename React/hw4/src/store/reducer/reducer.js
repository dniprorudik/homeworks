import { combineReducers } from 'redux';

const initialProds = {
  loading: false,
  prods: [],
  error: '',
};

const loadProdReducer = (store = initialProds, action) => {
  switch (action.type) {
    case 'REQUEST_PRODS_STARTED':
      return {
        ...store,
        loading: true,
      };
    case 'REQUEST_PRODS_SUCCESS':
      return {
        ...store,
        loading: false,
        error: null,
        prods: [...store.prods, ...action.payload],
      };
    case 'REQUEST_PRODS_FAILURE':
      return {
        ...store,
        loading: false,
        error: action.payload,
      };
    case 'LOADING':
      return {
        ...store,
        loading: true,
      };
    default:
      return store;
  }
};

const initialFavorite = {
  vendor: [],
};

if (JSON.parse(localStorage.getItem('vendorCode'))) {
  initialFavorite.vendor = JSON.parse(localStorage.getItem('vendorCode'));
}

const favoriteReducer = (store = initialFavorite, action) => {
  switch (action.type) {
    case 'ADD_FAVORITE':
      return {
        ...store,
        vendor: [...store.vendor, action.payload],
      };
    case 'REMOVE_FAVORITE':
      let arrFavorite = store.vendor;
      arrFavorite.splice(arrFavorite.indexOf(action.payload, 0), 1);
      return {
        ...store,
        vendor: arrFavorite,
      };
    default:
      return store;
  }
};

const initialBasket = {
  basket: [],
};

if (JSON.parse(localStorage.getItem('id'))) {
  initialBasket.basket = JSON.parse(localStorage.getItem('id'));
}

const basketReducer = (store = initialBasket, action) => {
  switch (action.type) {
    case 'ADD_BASKET':
      return {
        basket: [...store.basket, action.payload],
      };
    case 'REMOVE_BASKET':
      let arrBasket = store.basket;
      arrBasket.splice(arrBasket.indexOf(action.payload, 0), 1);
      return {
        basket: arrBasket,
      };
    default:
      return store;
  }
};

const initilaModal = {
  activeModal: false,
};

const modalReducer = (store = initilaModal, action) => {
  switch (action.type) {
    case 'MODAL':
      return {
        activeModal: !store.activeModal,
      };
    default:
      return store;
  }
};

export default combineReducers({
  loadProd: loadProdReducer,
  favorite: favoriteReducer,
  basket: basketReducer,
  modal: modalReducer,
});
