import axios from 'axios';

export const getProds = () => {
  return (dispatch) => {
    dispatch(getProdsStarted());
    axios('/prods.json')
      .then((res) => {
        dispatch(getProdsSuccess(res.data));
      })
      .catch((err) => {
        dispatch(getProdsFailure(err));
      });
  };
};

const getProdsStarted = () => ({
  type: 'REQUEST_PRODS_STARTED',
});

const getProdsSuccess = (prod) => ({
  type: 'REQUEST_PRODS_SUCCESS',
  payload: [...prod],
});

const getProdsFailure = (error) => ({
  type: 'REQUEST_PRODS_FAILURE',
  payload: { error },
});
