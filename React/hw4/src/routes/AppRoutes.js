import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import CardList from '../pages/CardList/CardList';
import FavoritesList from '../pages/Favorites/FavoritsList';
import BasketList from '../pages/Basket/BasketList';

function AppRoutes() {
  return (
    <Switch>
      <Redirect exact from="/" to="/main" />
      <Route exact path="/main" component={CardList} />
      <Route exact path="/basket" component={BasketList} />
      <Route exact path="/favorites" component={FavoritesList} />
      {/*<Route path='*' component={Page404} />*/}
    </Switch>
  );
}
export default AppRoutes;
