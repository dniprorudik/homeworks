import React, { useEffect } from 'react';
import './App.scss';
import Header from './components/Header/Header';
import AppRoutes from './routes/AppRoutes';
import { useDispatch } from 'react-redux';
import { getProds } from './store/actions/';

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({ type: 'LOADING' });
    setTimeout(() => {
      dispatch(getProds());
    }, 2000);
  }, []);

  return (
    <div>
      <Header />
      <AppRoutes></AppRoutes>
    </div>
  );
}

export default App;
