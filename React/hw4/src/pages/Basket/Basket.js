import React, { useState } from 'react';
import Card from '../../components/Card/Card';
import Modal from '../../components/Modal/Modal';
import Button from '../../components/Button/Button';
import { useSelector, useDispatch } from 'react-redux';

function Basket({ prods, removeBasket }) {
  const { activeModal } = useSelector((store) => {
    return store.modal;
  });
  const dispatch = useDispatch();

  const [id, setId] = useState('');
  const [name, setName] = useState('');

  const modalActive = (ProdID, ProdName) => {
    dispatch({ type: 'MODAL' });
    setName(ProdName);
    setId(ProdID);
  };

  const prodCard = prods.map((e) => (
    <Card
      key={e.id}
      id={e.id}
      name={e.name}
      url={e.url}
      price={e.price}
      vendorCode={e.vendorCode}
      color={e.color}
      removeBasket={modalActive}
      closeButton={true}
    />
  ));

  return (
    <>
      {prodCard}
      {activeModal && (
        <Modal
          textHeader={'Удалить из корзины?'}
          text={`Вы точно хотите удалить из корзины бас-гитару ${name}?`}
          setActive={modalActive}
          closeButton={true}
          actions={[
            <Button
              key="1"
              classStyle="modal__btn"
              text="Да"
              handClick={() => {
                removeBasket(id);
                dispatch({ type: 'MODAL' });
              }}
            />,
            <Button
              key="2"
              classStyle="modal__btn"
              text="Оставить в корзине"
              handClick={() => {
                dispatch({ type: 'MODAL' });
              }}
            />,
          ]}
        />
      )}
    </>
  );
}

export default Basket;
