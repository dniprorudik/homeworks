import React from 'react';
import Basket from './Basket';
import { useSelector, useDispatch } from 'react-redux';

function BasketList() {
  const { basket } = useSelector((store) => {
    return store.basket;
  });
  const { prods } = useSelector((store) => {
    return store.loadProd;
  });
  const dispatch = useDispatch();
  const removeBasket = (id) => {
    let arr = [];
    arr = JSON.parse(localStorage.getItem('id'));
    arr.splice(arr.indexOf(id, 0), 1);
    localStorage.setItem('id', JSON.stringify(arr));
    dispatch({ type: 'REMOVE_BASKET', payload: id });
  };

  let newArr = prods.filter((item) => basket.includes(item.id));
  return (
    <div className="body">
      <div className="body__container">
        <Basket prods={newArr} removeBasket={removeBasket} />
      </div>
    </div>
  );
}

export default BasketList;
