import React, { useState, useEffect } from 'react';
import './CardList.scss';
import Card from '../../components/Card/Card';
import PropTypes from 'prop-types';
import Loader from '../../components/Loader/Loader';
import Modal from '../../components/Modal/Modal';
import Button from '../../components/Button/Button';
import { useSelector, useDispatch } from 'react-redux';

function CardList() {
  const { prods = [{}], loading } = useSelector((store) => {
    return store.loadProd;
  });
  const { activeModal } = useSelector((store) => {
    return store.modal;
  });

  const dispatch = useDispatch();

  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [id, setId] = useState('');

  if (loading) {
    return <Loader />;
  }

  const modalActive = (ProdID, ProdName, ProdPrice) => {
    dispatch({ type: 'MODAL' });
    setName(ProdName);
    setPrice(ProdPrice);
    setId(ProdID);
  };

  const prodCard = prods.map((e) => (
    <Card
      key={e.id}
      id={e.id}
      name={e.name}
      url={e.url}
      price={e.price}
      vendorCode={e.vendorCode}
      color={e.color}
      handAdd={modalActive}
    />
  ));

  const addOrder = (ProdID) => {
    let arr = [];
    if (localStorage.getItem('id')) {
      arr = JSON.parse(localStorage.getItem('id'));
    }

    arr.push(ProdID);
    localStorage.setItem('id', JSON.stringify(arr));
    dispatch({ type: 'ADD_BASKET', payload: ProdID });
    dispatch({ type: 'MODAL' });
  };

  const removeOrder = (ProdID) => {
    let arr = [];
    if (localStorage.getItem('id')) {
      arr = JSON.parse(localStorage.getItem('id'));
      /*-Проверяем если нажали Cancel и такого элемента нет-*/
      if (arr.includes(ProdID, 0)) {
        arr.splice(arr.indexOf(ProdID, 0), 1);
        localStorage.setItem('id', JSON.stringify(arr));
        dispatch({ type: 'REMOVE_BASKET', payload: ProdID });
      }
    }

    dispatch({ type: 'MODAL' });
  };

  return (
    <div className="body">
      <div className="body__container">
        {prodCard}
        {activeModal && (
          <Modal
            textHeader={'Добавить в корзину?'}
            text={`Вы хотите купить бас-гитару ${name} всего лишь за ${price}?`}
            setActive={modalActive}
            closeButton={true}
            actions={[
              <Button
                key="1"
                classStyle="modal__btn"
                text="Ok"
                handClick={() => {
                  addOrder(id);
                }}
              />,
              <Button
                key="2"
                classStyle="modal__btn"
                text="Cancel"
                handClick={() => {
                  removeOrder(id);
                }}
              />,
            ]}
          />
        )}
      </div>
    </div>
  );
}

CardList.propTypes = {
  prodCard: PropTypes.node,
};

CardList.defaultProps = {
  prods: {
    id: '0',
    name: 'товар не найден',
    url: '/',
    price: '00.00',
    vendorCode: '0000',
    color: 'black',
  },
};

export default CardList;
