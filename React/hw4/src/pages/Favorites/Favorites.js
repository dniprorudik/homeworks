import React from 'react';
import Card from '../../components/Card/Card';

function Favorites({ prods, delFavorit }) {
  const prodCard = prods.map((e) => (
    <Card
      key={e.id}
      id={e.id}
      name={e.name}
      url={e.url}
      price={e.price}
      vendorCode={e.vendorCode}
      color={e.color}
      delFavorit={delFavorit}
    />
  ));

  return <>{prodCard}</>;
}

export default Favorites;
