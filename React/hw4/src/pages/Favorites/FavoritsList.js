import React from 'react';
import Favorites from './Favorites';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

function FavoritesList() {
  const { vendor = [] } = useSelector((store) => {
    return store.favorite;
  });
  const { prods = [{}] } = useSelector((store) => {
    return store.loadProd;
  });
  const dispatch = useDispatch();

  const delFavorit = (vendorCode) => {
    let arr = [];
    arr = JSON.parse(localStorage.getItem('vendorCode'));
    arr.splice(arr.indexOf(vendorCode, 0), 1);
    localStorage.setItem('vendorCode', JSON.stringify(arr));
    dispatch({ type: 'REMOVE_FAVORITE', payload: vendorCode });
  };

  let newArr = prods.filter((item) => vendor.includes(item.vendorCode));

  return (
    <div className="body">
      <div className="body__container">
        <Favorites prods={newArr} delFavorit={delFavorit} />
      </div>
    </div>
  );
}

FavoritesList.FavoritesList = {
  prods: PropTypes.array,
};

export default FavoritesList;
