import React, {useEffect, useState} from 'react';
import './App.scss';
import Header from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";
import axios from "axios";

function App() {
    const [prods, setProds] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(()=>{
        setTimeout(() => {
            axios('/prods.json')
                .then(res => {
                    setProds(res.data);
                    setIsLoading(false);
                })
        }, 1000)
    }, [])

    return (
        <div>
            <Header/>
            <AppRoutes prods={prods}  isLoading={isLoading}></AppRoutes>
        </div>
    );
}

export default App;
