import React, { useState, useEffect} from 'react'
import './Card.scss'
import Button from "../Button/Button";
import PropTypes from "prop-types";

function Card ({id, name, url, price, vendorCode, color, handAdd, closeButton, removeBasket, delFavorit}) {
    const [ favorites, setFavorites] = useState(false)

    useEffect(()=> {
        let arr = [];
        arr = JSON.parse(localStorage.getItem('vendorCode'));
        if(!!arr && arr.includes(vendorCode, 0)){
            setFavorites(true)
        }
    },[])


    let classStar = "card__star";
    if (favorites) {
        classStar += ' card__star_back'
    }

    const addFavorites =() =>{
        let arr = [];
        if (localStorage.getItem('vendorCode')){
            arr = JSON.parse(localStorage.getItem('vendorCode'));
        }

        arr.push(vendorCode);

        if (favorites){
            arr = JSON.parse(localStorage.getItem('vendorCode'));
            arr.splice(arr.indexOf(vendorCode, 0), 1);
            localStorage.setItem(
                "vendorCode",JSON.stringify(arr));
        }
        else {
            localStorage.setItem(
                "vendorCode",JSON.stringify(arr));
        }

        setFavorites(!favorites)
    }
    return (

        <div className="card">
            <h2 className="card__title">{name}</h2>
            {!closeButton && <div className={classStar} onClick={delFavorit? ()=>{delFavorit(vendorCode)} : addFavorites}></div>}
            {closeButton && <span className="card__close" onClick={()=>removeBasket(id, name)}></span>}
            <img className="card__img" src={url} alt={name}/>
            <span className="card__price">{price}</span>
            <span className="card__vendorCode">{vendorCode}</span>
            <span className="card__color">{color}</span>
            {!closeButton && <Button classStyle="card__btn" text="Add to cart" handClick={()=>handAdd(id, name, price)}/>}
        </div>
    )
}

Card.propTypes = {
    name: PropTypes.string,
    url: PropTypes.string,
    price: PropTypes.string,
    vendorCode: PropTypes.string,
    color: PropTypes.string,
    active: PropTypes.bool,
    favorites: PropTypes.bool,
    classStar: PropTypes.string,
}
Card.defaultProps  = {
    closeButton: false,
    removeBasket:undefined
}

export default Card
