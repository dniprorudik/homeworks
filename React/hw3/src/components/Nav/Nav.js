import React from 'react'
import { NavLink } from 'react-router-dom';
import './Nav.scss';

function Nav () {
    return (
        <ul>
            <li><NavLink exact to="/main" activeClassName='link__active'>Главная</NavLink></li>
            <li><NavLink exact to='/favorites' activeClassName='link__active'>Избранное</NavLink></li>
            <li><NavLink exact to='/basket' activeClassName='link__active'>Корзина </NavLink></li>
        </ul>
    )
}

export default Nav