import React, { PureComponent } from 'react'
import PropTypes from "prop-types";

function Button ({ classStyle, text, handClick}) {
    return (
        <button
            className={classStyle}
            onClick={handClick}
        >{text}
        </button>
    )
}
Button.propTypes = {
    classStyle: PropTypes.string,
    text: PropTypes.string,
    handClick: PropTypes.func,
}

Button.defaultProps = {
    text: 'Волшебство'
};

export default Button