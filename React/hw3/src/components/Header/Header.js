import React, {PureComponent} from 'react'
import './Header.scss'
import Nav from "../Nav/Nav";

function Header() {


    return (
        <div className="header">
            <div className="header__container">
                <img className="header__img" src="img/spanch_bass.jpg" alt="logo"/>
                <h1>Интернет магазин Бас гитар</h1>
                <Nav></Nav>
            </div>
        </div>
    )
}

export default Header