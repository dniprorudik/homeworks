
import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom';
import CardList from "../pages/CardList/CardList";
import FavoritesList from "../pages/Favorites/FavoritsList";
import BasketList from "../pages/Basket/BasketList";

function AppRoutes({prods, isLoading}) {
    return (
        <Switch>
            <Redirect exact from='/' to='/main' />
            <Route exact path="/main"><CardList prods={prods} isLoading={isLoading}></CardList></Route>
            <Route exact path="/basket"><BasketList prods={prods}></BasketList></Route>
            <Route exact path="/favorites"><FavoritesList prods={prods}></FavoritesList></Route>
            {/*<Route path='*' component={Page404} />*/}
        </Switch>
    )
}
export default AppRoutes
