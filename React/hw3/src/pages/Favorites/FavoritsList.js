import React, {useEffect, useState} from 'react'
import Favorites from "./Favorites";
import PropTypes from "prop-types";

function FavoritesList({prods}) {
    const [favorit, setFavorit] = useState([]);

    useEffect(() => {
        setFavorit(JSON.parse(localStorage.getItem('vendorCode')));
    },[]);

    const delFavorit = (vendorCode) => {
        let arr = [];
        arr = JSON.parse(localStorage.getItem('vendorCode'));
        arr.splice(arr.indexOf(vendorCode, 0), 1);
        localStorage.setItem(
            "vendorCode", JSON.stringify(arr));
        setFavorit(JSON.parse(localStorage.getItem('vendorCode')));
    }

    let newArr = prods.filter((item) =>favorit.includes(item.vendorCode))

    return (
        <div className="body">
            <div className="body__container">
                <Favorites prods={newArr} delFavorit={delFavorit} />
            </div>
        </div>
    )
}

FavoritesList.FavoritesList = {
    prods: PropTypes.array,
}

export default FavoritesList

