import React, {useEffect, useState} from 'react'
import Basket from "./Basket";

function BasketList({prods}) {
    const [basket, setBasket] = useState([])

    useEffect(() => {
        setBasket(JSON.parse(localStorage.getItem('id')));
    },[]);

    const removeBasket = (id) => {
        let arr = [];
        arr = JSON.parse(localStorage.getItem('id'));
        arr.splice(arr.indexOf(id, 0), 1);
        localStorage.setItem(
            "id", JSON.stringify(arr));
        setBasket(JSON.parse(localStorage.getItem('id')));
    }

    let newArr = prods.filter((item) =>basket.includes(item.id))
    return (
        <div className="body">
            <div className="body__container">
                <Basket prods={newArr} removeBasket={removeBasket}/>
            </div>
        </div>
    )

}

export default BasketList