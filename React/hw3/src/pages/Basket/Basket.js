import React, {useState} from 'react'
import Card from "../../components/Card/Card";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";

function Basket({prods, removeBasket}) {
    const [active, setActive] = useState(false);
    const [id, setId] = useState('');
    const [name, setName] = useState('');

    const modalActive = (ProdID, ProdName) => {
        setActive(!active);
        setName(ProdName);
        setId(ProdID);
    }

    const prodCard = prods.map((e) => <Card key={e.id} id={e.id} name={e.name} url={e.url} price={e.price}
                                            vendorCode={e.vendorCode}
                                            color={e.color} removeBasket={modalActive} closeButton={true}/>
    )

    return (
        <>
            {prodCard}
            {active && <Modal textHeader={"Удалить их корзины?"}
                              text={`Вы точно хотите удалить из корзины бас-гитару ${name}?`}
                              active={active}
                              setActive={modalActive}
                              closeButton={true}
                              actions={[<Button key="1" classStyle="modal__btn" text="Да"
                                                handClick={() => {
                                                    removeBasket(id);
                                                    setActive(false)
                                                }}/>,
                                  <Button key="2" classStyle="modal__btn" text="Оставить в корзине"
                                          handClick={() => {
                                              setActive(false)
                                          }}/>]}
            />}

        </>
    )
}

export default Basket