import React, { useState, useEffect} from 'react'
import './CardList.scss'
import Card from "../../components/Card/Card"
import PropTypes from "prop-types";
import Loader from "../../components/Loader/Loader";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";


function CardList ({prods, isLoading}) {
    const [active, setActive] = useState(false);

    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [id, setId] = useState('');

    if (isLoading) {
        return <Loader />
    }

    const modalActive = (ProdID, ProdName, ProdPrice) => {
        setActive(!active);
        setName(ProdName);
        setPrice(ProdPrice);
        setId(ProdID);
    }

    const prodCard = prods.map(e => <Card key={e.id} id={e.id} name={e.name} url={e.url} price={e.price} vendorCode={e.vendorCode} color={e.color} handAdd={modalActive} />)

    const addOrder = (ProdID) => {

        let arr = [];
        if (localStorage.getItem('id')){
            arr = JSON.parse(localStorage.getItem('id'));
        }

        arr.push(ProdID);
        localStorage.setItem(
            "id",JSON.stringify(arr));

        setActive(!active);
    }

    const  removeOrder = (ProdID) =>{

        let arr = [];
        if (localStorage.getItem('id')){
            arr = JSON.parse(localStorage.getItem('id'));
            console.log(arr.indexOf(ProdID, 0));
            /*-Проверяем если нажали Cancel и такого элемента нет-*/
            if (arr.includes(ProdID, 0)){
                arr.splice(arr.indexOf(ProdID, 0), 1);
                localStorage.setItem(
                    "id",JSON.stringify(arr));}
        }

        setActive(!active);
    }

    return (
        <div className="body">
            <div className="body__container">
                {prodCard}
                {active && <Modal textHeader={"Добавить в корзину?"}
                                  text={`Вы хотите купить бас-гитару ${name} всего лишь за ${price}?`}
                                  active={active}
                                  setActive={modalActive}
                                  closeButton={true}
                                  actions={[<Button key="1" classStyle="modal__btn" text="Ok"
                                                    handClick={() => {addOrder(id)}}/>,
                                      <Button key="2" classStyle="modal__btn" text="Cancel"
                                              handClick={() => {removeOrder(id)}}/>]}
                />}
            </div>
        </div>
    )
}

CardList.propTypes = {
    prodCard: PropTypes.node,
}

CardList.defaultProps = {
    prods: {
        id: "0",
        name: "товар не найден",
        url: "/",
        price: "00.00",
        vendorCode: "0000",
        color: "black"
    }
};

export default CardList