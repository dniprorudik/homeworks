import React from 'react';
import './App.scss';
import Button from "./components/Button";
import Modal from "./components/Modal";

class App extends React.Component {
    state = {
        active1: false,
        active2: false,
    }
    activeHandler1 = () => {
        this.setState(state => ({
            active1: !this.state.active1
        }))
    }
    activeHandler2 = () => {
        this.setState(state => ({
            active2: !this.state.active2
        }))
    }

    render() {
        return (
            <div>

                <Button backgroundColor="blue" text="Open first modal" handClick={this.activeHandler1}/>
                <Button backgroundColor="green" text="Open second modal" handClick={this.activeHandler2}/>
                {this.state.active1 && <Modal textHeader={"Do you want to delete this file?"}
                                              text={"Once you delete this file, it won't be possible to undo thus action." +
                                              " Are you sure you want to delete it?"}
                                              active={this.state.active1}
                                              setActive={this.activeHandler1}
                                              closeButton={true}
                                              actions={[<Button key="1" backgroundColor="#B53726" text="Ok"
                                                                handClick={this.activeHandler1}/>,
                                                        <Button key="2" backgroundColor="#B53726" text="Cancel"
                                                          handClick={this.activeHandler1}/>]}
                />}
                {this.state.active2 && <Modal textHeader={"Do you want to create new file?"}
                                              text={"Once you delete this file, it won't be possible to undo thus action." +
                                              " Are you sure you want to create it?"} active={this.state.active2}
                                              setActive={this.activeHandler2} closeButton={false}
                                              actions={[<Button key="3" backgroundColor="#B53726" text="Ok"
                                                                handClick={this.activeHandler2}/>,
                                                        <Button key="4" backgroundColor="#B53726" text="Cancel"
                                                          handClick={this.activeHandler2}/>]}
                />}
            </div>
        );
    }
}

export default App;
