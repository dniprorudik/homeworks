import React from 'react';

class Modal extends React.Component {
    render() {
        const {text, textHeader, active, setActive, closeButton, actions} = this.props;
        return (
            <div className={"modal active"} onClick={setActive}>
                <div className="modal__content" onClick={e=>e.stopPropagation()}>
                    <div className="modal__header">{textHeader}</div>
                    <span className="modal__close" onClick={setActive} style={closeButton ? {} : {display: "none"}}></span>
                    <p className="modal__text">{text}</p>
                    <div className="modal__section-btn">
                        {actions}
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal