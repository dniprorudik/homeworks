import React from 'react';
import Modal from "./Modal";

class Button extends React.Component{
    render() {
        const {backgroundColor, text, handClick} = this.props
        return (
            <button
                className={"btn"}
                style={{backgroundColor: backgroundColor}}
                onClick={handClick}
            >{text}
            </button>
        )
    }
}

export default Button