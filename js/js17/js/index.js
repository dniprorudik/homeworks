"use strict";

let student = {
    name: null,
    lastName: null,
    tabel: {},
};
student.name = prompt("Введите имя студента", "Алеша");
student.lastName = prompt("Введите фамилию студента", "Попович");

while (true) {
    let courseName = prompt("Введите предмет студента");
    if (!courseName) {
        break;
    }
    let estimate = prompt(`Введите оценку по предменту ${courseName}`);
    student.tabel[courseName] = estimate;

}

let count = 0;
let countBad = 0;
let sum = 0;
for (let key in student.tabel) {
    if (student.tabel[key] < 4) {
        countBad++;
    }
    sum += student.tabel[key];
    count++;
}

if (countBad === 0) {
    alert("Студент переведен на следующий курс");
}
let avrSum = sum / count;
if (avrSum > 7) {
    alert("Студенту назначена стипендия");
}

console.log(student);
console.log(student.tabel);