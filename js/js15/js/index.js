"use strict";

let n = prompt('enter the number');

function check(num) {
    return (!Number.isInteger(+num) || num === '');
}

while (check(n)) {
    n = +prompt('enter the number', n);
}


function factorial(n) {
    if (n === 1) {
        return 1
    } else {
        return n * factorial(n - 1);
    }
}
console.log(factorial(n));