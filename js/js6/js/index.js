"use strict";

let array =  ['hello', 'world', 23, '23', null];
let type = 'string';

function filterBy(array, type) {
    return array.filter(item => typeof(item) !== type);
}
console.log(filterBy(array, type))