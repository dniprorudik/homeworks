"use strict";
/*-создаем таблицу-*/
let table = document.createElement('table');
table.className = "place";
document.body.append(table);
/*-Создаем поле-*/
for (let i = 0; i < 10; i++) {
    let tr = document.createElement('tr');
    table.append(tr);
    for (let j = 0; j < 10; j++) {
        let td = document.createElement('td');
        tr.append(td);
    }
}
/*-слушаем ячейки таблицы и добавляем им класс черный-*/
table.addEventListener('click', (e) => {
    let el = e.target;
    el.classList.toggle('black');
    e.stopPropagation()
})
/*-Слушаем документ и добавляем таблице класс черный, а ячейкам класс белый-*/
document.addEventListener('click', (e) => {
    let ClassBlack = document.querySelectorAll('.black');
    let ClassWhite = document.querySelectorAll('.white');
    table.classList.toggle('black');
    for (let square of ClassBlack) {
         square.classList.remove("black");
         square.className = ('white');
    }
    for (let square of ClassWhite) {
        square.classList.remove("white");
        square.className = ('black');
    }
})