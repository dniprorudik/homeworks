"use strict";

function createNewUser() {
    let firstName = prompt("Введите имя", "Алеша");
    let lastName = prompt("Введите фамилию", "Попович");
    let newUser = {
        firstName,
        lastName,
        getLogin() {
            return this.firstName[0].toLowerCase()+lastName.toLowerCase();
        },
        setFirstName(valueFirstName) {
            this.firstName = valueFirstName;
        },
        setLastName(valueLastName) {
            this.lastName = valueLastName;
        }
    };
    return newUser;
}

let user = createNewUser()
console.log(user.getLogin());
user.setFirstName("Ваня");
console.log(user);