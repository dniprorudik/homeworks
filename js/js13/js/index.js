"use strict";

let btn = document.querySelector('.btn');
let bodyStyle = document.head.querySelector('.style');
/*-Если уже есть данные в localStorage то считываем их-*/
if (localStorage.getItem('bodyStyle') !== null) {
    bodyStyle.href = localStorage.getItem('bodyStyle');
}
/*-Слушаем кнопку для изменения стиля-*/
btn.addEventListener('click', () => {
        if (bodyStyle.href.includes('css/style.css')) {
            bodyStyle.href = 'css/style2.css'
        } else {
            bodyStyle.href = 'css/style.css'
        }
/*-Записываем тему в localStorage-*/
        localStorage.setItem('bodyStyle', bodyStyle.href);
    }
)

