"use strict";
let eye = document.querySelector('.eye');
let eyeSlash = document.querySelector('.eyeSlash');
let pass = document.querySelector('.pass');
let rePass = document.querySelector('.re-pass');
let btn = document.querySelector('.btn');
let form = document.querySelector('.password-form');
/*-прослушивание поля пароль-*/
eye.addEventListener('click', () => {
    change(pass, eye)
})
/*-Прослушивание поля повтора пароля-*/
eyeSlash.addEventListener('click', () => {
    change(rePass, eyeSlash)
})

form.addEventListener('submit', (e) => {
    e.preventDefault();
    compare(pass.value, rePass.value);
})

pass.onfocus = removeError; /*-Удаляем сообщение об ошибке при наведении фокуса на пассворд-*/
rePass.onfocus = removeError;/*-Удаляем сообщение об ошибке при наведении фокуса на ре-пассворд-*/

function removeError() {
    /*-если есть значение о ошибке-*/
    if (Boolean(document.querySelector('.err'))) {
        document.querySelector('.err').remove();
    }
}

function change(pass, eye) {
    /*- Смена иконки и типа-*/
    if (pass.type == 'text') {
        pass.type = 'password';
    } else {
        pass.type = 'text'
    }
    eye.classList.toggle('fa-eye-slash');
    eye.classList.toggle('fa-eye');
}

function compare(value1, value2) {
    /*-сравнение-*/
    if ((value1 === value2) && value1 !== '') {
        alert('You are welcome');
    } else if (!document.querySelector('.err')) {
        document.querySelector('.re-pass-label').insertAdjacentHTML('beforeend', `<span class="err" style="display:block; color:red">Нужно ввести одинаковые значения </span>`);

    }
}

