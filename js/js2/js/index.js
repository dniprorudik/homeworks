"use strict";

/*--------Первая часть задания---------------*/

let num = prompt('Enter the number');
while (!Number.isInteger(+num) || num ===''){ //Проверка на корректность числа
    num = prompt('Enter the number +');
}
if (num > -4 && num < 0 || num < 4 && num > 0) { //Проверка на наличие кратных чисел
    alert('Sorry, no numbers');
} else {
    if (num > 0) {
        for (let i = 1; i <= num; i++) { //положительные кратные числа
            if (i % 5 === 0) {
                console.log(i);
            }
        }
    } else {
        for (let i = -1; i >= num; i--) {//отричательные кратные числа
            if (i % 5 === 0) {
                console.log(i);
            }
        }
    }
    alert('Look at the console');
}

