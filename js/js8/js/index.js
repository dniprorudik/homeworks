"use strict";

let form = document.querySelector('.form');
let fieldPrice = document.querySelector('.field-price');
let value;

/*--события при фокусе---*/
fieldPrice.onfocus = function () {
    fieldPrice.className = "wrapper";
    if (Boolean(document.querySelector('.current-price'))) { /*-если есть значение выше - удаляем-*/
        document.querySelector('.current-price').remove();
    }
    if (Boolean(document.querySelector('.current-negative'))) { /*-если есть значение ниже - удаляем-*/
        document.querySelector('.current-negative').remove();
    }
};

/*--события убрали фокус---*/
fieldPrice.onblur = function () {
    value = fieldPrice.value;
    if (value > 0) { /*-если больше нуля-*/
        fieldPrice.classList.toggle('wrapper');
        fieldPrice.className = "wrapper-positive";
        form.insertAdjacentHTML('beforebegin', `<span class="current-price">Текущая цена: ${value} <span class="btn">&otimes;</span></span>`);
        document.querySelector('.btn').addEventListener('click', (e) => {
            document.querySelector('.current-price').remove();
            fieldPrice.value = 0;
        });
    } else {/*-если меньше нуля-*/
        fieldPrice.className = "wrapper-negative";
        form.insertAdjacentHTML('beforeend', `<span class="current-negative">Please enter correct price</span>`);
    }
};
