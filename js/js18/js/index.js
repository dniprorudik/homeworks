"use strict";

let obj = {
    name: "Григорий",
    lastName: "Костышин",
    dimensions: [5, 6, 7],
    tabel: {
        mat: 5,
        lit: "Есенин",
        money: [10, 11, 12],
        flowers: {
            romashka: 10,
            tulpan: 15,
            poliv: [1, 5, 10],
        }
    }
}
let newObj = new Object();

function copy(obj, newObj) {
    for (let key in obj) {
        if (typeof obj[key] !== "object") {
            newObj[key] = obj[key];
        }
        if (Array.isArray(obj[key])) {
            newObj[key] = obj[key].slice();
        }
        if (typeof obj[key] === "object" && !Array.isArray(obj[key])) {
            newObj[key] = obj[key];
            copy(obj[key], newObj[key]);

        }
    }
}

copy(obj, newObj);

console.log(obj);
console.log(newObj);