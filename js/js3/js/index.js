"use strict";

function checkOperand(operand) {
    return ((operand !== '/' && operand !== '*' && operand !== '-' && operand !== '+') || operand === '');
}

function checkNumber(num){
    while (!Number.isInteger(+num) || num === '') {
        num = prompt('enter the number');
    }
    num = Number(num);
    return num;
}

function calc(a, b, op) {
    switch (op) {
        case '/':
            return a / b;
        case '*':
            return a * b;
        case '-':
            return a - b;
        case '+':
            return a + b;
    }
}

let a = prompt('enter the first number');
a = checkNumber(a);
let b = prompt('enter the second number');
b = checkNumber(b);

let op = prompt('enter operand (/, *. -, +)');
while (checkOperand(op)) {
    op = prompt('enter operand (/, *. -, +)');
}

console.log(calc(a, b, op));