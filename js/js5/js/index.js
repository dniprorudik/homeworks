"use strict";

function createNewUser() {
    let firstName = prompt("Введите имя", "Алеша");
    let lastName = prompt("Введите фамилию", "Попович");
    let birthday = prompt("Введите день рождения (текст в формате dd.mm.yyyy)", "01.01.2001");

    function checkB(valueDay) {
        let bool = false;
        let arrayMM = [0, 31, 28, 31, 30, 31, 30, 31, 30, 31, 30, 31, 30]
        let dd = valueDay.slice(0, valueDay.indexOf('.'));
        let mm = valueDay.slice(valueDay.indexOf('.') + 1, valueDay.indexOf('.', valueDay.indexOf('.') + 1));
        let yyyy = valueDay.slice(valueDay.indexOf('.', valueDay.indexOf('.') + 1) + 1);
        /*-Проверка на корректность данных, верная точка, и количество символов-*/
        if (dd.length !== 2 || mm.length !== 2 || yyyy.length !== 4 || Number.isNaN(Number(dd)) || Number.isNaN(Number(mm)) || Number.isNaN(Number(yyyy))) {
            bool = true;
        } else if (Number(mm) < 1 || Number(mm) > 12) { /*-Проверка на месяцы-*/
            bool = true;
        } else if ((Number(mm) === 2 && Number(dd) >= 1 && Number(dd) <= 29 && Number(yyyy) % 4 === 0)) {/*-Проверка на высокосный февраль-*/
            bool = false;
        } else if (!(Number(dd) >= 1 && Number(dd) <= arrayMM[Number(mm)] /*&& Number(yyyy) % 4 !== 0*/)) {/*-Проверка на дни -*/
            bool = true;
        }
        return bool  /*-Если что то введено не правильно возвращает true-*/
    }

    function checkBirthday(day) {
        while (checkB(day)) {
            day = prompt("не верный формат (текст в формате dd.mm.yyyy)", "01.01.2001");
        }
        return day
    }

    let newUser = {
        firstName,
        lastName,
        birthday: checkBirthday(birthday),
        getLogin() {
            return this.firstName[0].toLowerCase() + lastName.toLowerCase();
        },
        getAge() {
            let dd = this.birthday.slice(0, this.birthday.indexOf('.'));
            let mm = this.birthday.slice(this.birthday.indexOf('.') + 1, this.birthday.indexOf('.', this.birthday.indexOf('.') + 1));
            let yyyy = this.birthday.slice(this.birthday.indexOf('.', this.birthday.indexOf('.') + 1) + 1);
            let today = new Date();
            let Birthday = new Date(yyyy, mm - 1, dd);
            let thisBirthday = new Date(today.getFullYear(), mm - 1, dd);
            let age = today.getFullYear() - Birthday.getFullYear();
            if (today < thisBirthday) {
                age--
            }
            return age;
        },
        getPassword() {
            let yyyy = this.birthday.slice(this.birthday.indexOf('.', this.birthday.indexOf('.') + 1) + 1);
            return this.firstName[0].toUpperCase() + lastName.toLowerCase() + yyyy;
        },
        setFirstName(valueFirstName) {
            this.firstName = valueFirstName;
        },
        setLastName(valueLastName) {
            this.lastName = valueLastName;
        },

    };
    return newUser;
}

let user = createNewUser()
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());