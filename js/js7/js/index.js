"use strict";

let array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

function arrfunc(array) {
    let newArray = array.map(item => `<li> ${item} </li>`)
    let ol = document.createElement('ol');
    ol.className = "alert";
    ol.innerHTML = newArray.join(' ');
    console.log(ol.innerHTML);
    return document.body.append(ol);
}

arrfunc(array);


