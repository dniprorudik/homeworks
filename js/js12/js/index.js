"use strict";

let img = document.querySelectorAll('.images-wrapper > img');
let i = 0;
/*-Запускаем показ картинок-*/

let intervalID;
changeImg();
i++;
intervalID = setInterval(changeImg, 2000)

function changeImg() {
    /*-Функция показа картинок-*/
    for (let el of img) {
        el.classList.remove('enable');
    }
    img[i].classList.toggle('enable');
    i++;
    if (i === img.length) {
        i = 0;
    }
}

function startID() {
    /*-Функция продолжить показ. Удаляем слушателя, что б не было ускорения-*/
    intervalID = setInterval(changeImg, 2000)
    btnStart.removeEventListener("click", startID);
}

/*-Создаем кнопку стоп-*/
let btnStop = document.createElement('button');
btnStop.className = "btn";
btnStop.innerText = 'Прекратить'
document.body.append(btnStop);

/*-Создаем кнопку старт-*/
let btnStart = document.createElement('button');
btnStart.className = "btn";
btnStart.innerText = 'Возобновить показ';
document.body.append(btnStart);

/*-Слушаем кнопку стоп-*/
btnStop.addEventListener('click', () => {
    clearInterval(intervalID);
    btnStart.addEventListener('click', startID)
})

/*-Слушаем кнопку старт-*/
btnStart.addEventListener('click', startID)
