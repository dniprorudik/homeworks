"use strict";

let ulHead = document.querySelector('.tabs');
let ulHeadItems = document.querySelectorAll('.tabs-title');
let ul = document.querySelector('.tabs-content');
let li = ul.querySelectorAll('li');
let n = ulHeadItems[0].dataset.id;
let i = 0;

/*-Делаем все невидимые-*/
for (let i of li) {
    i.hidden = true;
}
/*-Выставляем значение первому элементу-*/
li[n].hidden = false;
ulHeadItems[i].classList.toggle('head');

/*-Слушаем клик по вкладке -*/
ulHead.addEventListener('click', (e) => {
    let head = e.target;
    head.classList.toggle('head');
    ulHeadItems[i].classList.toggle('head');
    for (let j= 0 ; j < ulHeadItems.length; j++){ /*-Нужно получить индекс элемента с нужным data-id-*/
        if (ulHeadItems[j].dataset.id === e.target.dataset.id) {
            i = j;
        }
    }
    let num = e.target.dataset.id;
    li[n].hidden = true;
    n = num;
    li[num].hidden = false;
});
