"use strict";

let values = document.querySelectorAll('.btn');
document.addEventListener('keydown', (event) => {
    changeColor(event.key);
})

function changeColor(key) {
    for (let i of values) {
        i.classList.remove('color');
        if (i.innerHTML.toUpperCase() === key.toUpperCase()) {
            i.classList.toggle('color');
        }
    }
}
