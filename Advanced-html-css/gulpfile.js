const gulp = require('gulp');
const styles = require('./gulp/style');
const clean = require('gulp-clean');
const images = require('./gulp/image');
const jsCompress = require('./gulp/jscript');
const html = require("./gulp/html");
const server = require('./gulp/server');
const reset = require('./gulp/reset');



function cleanAll() {
    return gulp.src('dist/**/*.*')
        .pipe(clean({
            read: false,
        }))
}

const build = gulp.series(cleanAll, html, reset, styles, images, jsCompress);

module.exports.build = build;
module.exports.dev = gulp.series(build, server);