"use strict";

let burger = document.querySelector(".head__burger");
let menu = document.querySelector(".head__nav");

burger.addEventListener('click', () => {
    burger.classList.toggle('active');
    menu.classList.toggle('menu-active');
})