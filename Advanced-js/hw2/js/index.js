"use strict"

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function Error(message) {
    this.messsage = message;
    this.name = "Error message";
}

function diffArray(arr, checkArr) {
    /*-сравниваем массивы-*/
    let prop = [...checkArr.filter(el => !arr.includes(el))]
    throw new Error(`Нет ключа ${prop}`)
}

let checkArr = ['author', 'name', 'price']

let div = document.createElement('div');
div.id = "root";
document.body.append(div);
let ul = document.createElement("ul");
div.append(ul);

function mapping(book) {
    let li = document.createElement("li");
    let {author, name, price} = book;
    li.innerHTML = `${author} "${name}" - ${price}`
    ul.append(li);
}

function checkingCorrectData(books) {
    for (const book of books) {
        let arrKeys = Object.keys(book);
        if (arrKeys.length < 3) {
            diffArray(arrKeys, checkArr);
        } else {
            mapping(book)
        }
    }
}

try {
    checkingCorrectData(books);
} catch (e) {
    console.error(e)
}