 "use strict"

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get getName() {
        return this.name;
    }
    get getAge() {
        return this.age;
    }
    get getSalary() {
        return this.salary;
    }
    set setName(value) {
        this.name = value;
    }
    set setAge(value) {
        this.age = value;
    }
    set setSalary(value) {
        this.salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get getLang() {
        return this.salary;
    }
    set setLang(value) {
        this.name = value;
    }
    get getSalary() {
        return 3 * this.salary;
    }
}
let Ivan = new Programmer('Ivan', 30,10000, ['js','T-SQL']);
let Alex = new Programmer('Alex', 33,12000, ['js','T-SQL']);
let Yura = new Programmer('Yura', 24,3000, ['js','T-SQL']);

console.log(Ivan);
console.log(Ivan.getAge);
Ivan.setSalary= 14302;
console.log(Ivan.getSalary);
console.log(Ivan.getLang);
console.log(Alex);
console.log(Yura);

