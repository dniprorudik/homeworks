"use strict"

let k = 0; /*-счетчик-*/
let speed = null;

/*-создаем таблицу-*/
let playingField = document.querySelector('.playing_field');
let table = document.createElement('table');
table.className = "place";
playingField.append(table);

/*-Создаем поле-*/
for (let i = 1; i <= 10; i++) {
    let tr = document.createElement('tr');
    tr.className = `row ${i}`
    table.append(tr);
    for (let j = 1; j <= 10; j++) {
        let td = document.createElement('td');
        k++;
        td.className = `td${k}`
        tr.append(td);
    }
}
let arr = [];
for (let i = 1; i <= k; i++) {
    arr.push(i);
}

/*-создаем кнопку-*/
let levelField = document.querySelector('.level');
let easeButton = document.createElement('button');
easeButton.className = "ease-btn";
easeButton.innerHTML = "Легкий"
levelField.append(easeButton);

let normalButton = document.createElement('button');
normalButton.className = "normal-btn";
normalButton.innerHTML = "Средний"
levelField.append(normalButton);

let hardButton = document.createElement('button');
hardButton.className = "hard-btn";
hardButton.innerHTML = "Тяжелый"
levelField.append(hardButton);


/*-выбираем скорость игры и только после этого стартуем-*/
levelField.addEventListener('click', (e)=>{
    let event = e.target;
    switch (event.className) {
        case 'ease-btn': speed = 1500;
        break
        case 'normal-btn': speed = 1000;
        break
        case 'hard-btn': speed = 500;
        break
    }
    console.log(speed);
    /*-кнопка старт-*/
    button.addEventListener('click', (e) => {
        start()
    })
})

let customField = document.querySelector('.custom_field');
let button = document.createElement('button');
button.className = "button";
button.innerHTML = "Старт"
customField.append(button);

let compTxt = document.createElement('p');
let compPoint = document.createElement('span');
compPoint.id = "comp";
compTxt.innerText = "Комп очки:"
customField.append(compTxt);
compTxt.append(compPoint);


let humanTxt = document.createElement('p');
let humanPoint = document.createElement('span');
humanPoint.id = "human";
humanTxt.innerText = "Человек очки:"
customField.append(humanTxt);
humanTxt.append(humanPoint);

class Player {
    constructor(name, point) {
        this.name = name;
        this.point = point;
    }
    get getPoint() {
        return this.point;
    }

     setPoint(value) {
        console.log(this.point);
        this.point = this.point + value;
    }

    changeHTML(number = this.point) {
        let element = document.querySelector(  `#${this.name}`);
        element.innerText = number;
    }
}

let comp = new Player('comp', 0);
let human = new Player('human', 0);

/*-создаем класс, что б использовать его для слушателя по квадрату и логирования-*/
class Menu {
    constructor(square) {
        this.square = square;
    }

    handleEvent(event) {
        let tr = event.target;
        if (tr.className == this.square.className) {
            tr.classList.remove("blue");
            tr.className = 'green';
            human.setPoint(1);
            human.changeHTML();
        }
    }
}

/*-получаем случайное число в заданном интервале-*/
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //Максимум и минимум включаются
}

function compare() {
   if (comp.point> human.point) {
       return comp.name
   } else {
       return human.name
   }
}

/*-функция старта-*/
function start() {
    let randomNumber = getRandomIntInclusive(0, arr.length - 1);
    let square = document.querySelector(`.td${arr[randomNumber]}`);
    square.classList.toggle('blue');
    arr.splice(randomNumber, 1);

    /*-вешаем слушателя на таблицу-*/
    let menu = new Menu(square);
    table.addEventListener('click', menu)

/*-таймер-*/
    setTimeout(() => {
        table.removeEventListener('click', menu)
        // console.log(square.className.includes('blue'));
        if (square.className.includes('blue')) {
            square.classList.remove("blue");
            square.className = 'red'
            comp.setPoint(1);
            comp.changeHTML();
        }
        if (comp.getPoint !== 51 && human.getPoint !== 51){
            start();
        }
        else {
            let reloadButton = document.createElement('button');
            reloadButton.className = "reload-btn";
            reloadButton.innerHTML = "Запустить заново"
            playingField.innerHTML = `Победил ${compare()}`
            playingField.append(reloadButton);
            reloadButton.addEventListener('click', ()=>{
                location.reload();
            })
        }
    }, speed)
}


