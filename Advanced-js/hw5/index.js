"use strict"

let urlGetIP = 'http://api.ipify.org/?format=json';
let urlFindIP = 'http://ip-api.com/';



let btn = document.querySelector('.glow-on-hover');

let div = document.createElement('div');
div.id = "root";
document.body.append(div);

btn.addEventListener('click', () => {
    fetch(urlGetIP)
    .then((response) => {
        return response.json()
    })
        .then(dataIP => {
            console.log(dataIP);
            urlFindIP = urlFindIP + 'json/' + dataIP.ip
            console.log(urlFindIP);
            fetch(urlFindIP).then(response => {
                return response.json()
            }).then((data) => {
                let template = `
                    <div class="card">
                        <p>континент ${data.timezone}</p>
                        <p>страна ${data.country}</p>
                        <p>регион ${data.regionName}</p>
                        <p>город ${data.city}</p>       
                    </div>`;

                div.insertAdjacentHTML('beforeend', template);
            })
        })
})
