"use strict"

const requestURL = 'https://swapi.dev/api/films';

async function sendRequest(url) {
    return await fetch(url).then((Response) => {
        return Response.json()
    })
}

let ul = document.createElement('ul');
document.body.append(ul);

sendRequest(requestURL).then((data) => {
    for (let el of data.results) {
        let {title, episode_id, opening_crawl, characters} = el
        let li = document.createElement('li');
        li.innerHTML = `Episode - ${episode_id}; Film - ${title}; Description - ${opening_crawl}`;
        ul.append(li);
        let ulsp = document.createElement('ul');
        li.append(ulsp);
        characters.map((key) => {
            sendRequest(key).then((list) => {
                let {name, height, mass, hair_color, skin_color, eye_color} = list;
                let lisup = document.createElement('li');
                lisup.innerHTML = `<b>Name</b> - ${name}, <b>Mass</b> - ${mass}, <b>Hair color</b> - ${hair_color}, <b>Skin color</b> - ${skin_color}, <b>Eye color</b> - ${eye_color}`;
                ulsp.append(lisup);
            })
        })
    }
})

